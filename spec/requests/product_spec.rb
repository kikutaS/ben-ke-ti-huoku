require 'rails_helper'

RSpec.describe "Product", type: :request do
  let(:product) { create(:base_product) }

  describe "GET #show" do
    before do
      get potepan_product_url product.id
    end

    it "should success request" do
      expect(response.status).to eq 200
    end
  end
end
